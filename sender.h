#pragma once

#include <asio.hpp>
#include <chrono>
#include <string>

class UdpSender {
public:
    UdpSender(const std::string& target_ip, unsigned short port, int period_ms);

    void start_sending();
    void stop_sending();

    unsigned int get_sent_datagrams() const;

private:
    void send_next_datagram();

    asio::io_context io_context_;
    asio::ip::udp::socket socket_;
    asio::ip::udp::endpoint target_endpoint_;
    asio::steady_timer timer_;
    int period_ms_;
    unsigned int sent_datagrams_;
};