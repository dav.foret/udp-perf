// Include the header file for the UdpSender class.
#include "sender.h"

// Constructor for the UdpSender class.
UdpSender::UdpSender(const std::string& target_ip, unsigned short port, int period_ms)
    : socket_(io_context_),
    target_endpoint_(asio::ip::make_address(target_ip), port),
    timer_(io_context_),
    period_ms_(period_ms),
    sent_datagrams_(0) {
    // Open a UDP socket with IPv4 addressing.
    socket_.open(asio::ip::udp::v4());
}

// Start sending datagrams.
void UdpSender::start_sending() {
    send_next_datagram();
    io_context_.run();
}

// Stop sending datagrams.
void UdpSender::stop_sending() {
    io_context_.stop();
}

// Get the number of sent datagrams.
unsigned int UdpSender::get_sent_datagrams() const {
    return sent_datagrams_;
}

// Send the next datagram.
void UdpSender::send_next_datagram() {
    socket_.async_send_to(asio::buffer(&sent_datagrams_, sizeof(sent_datagrams_)), target_endpoint_,
        [this](std::error_code ec, std::size_t) {
            // If there's no error, increment the sent_datagrams counter,
            // set the timer for the next send, and call send_next_datagram again.
            if (!ec) {
                ++sent_datagrams_;
                timer_.expires_after(std::chrono::milliseconds(period_ms_));
                timer_.async_wait([this](const std::error_code&) { send_next_datagram(); });
            }
        });
}