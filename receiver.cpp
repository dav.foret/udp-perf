// Include the header file for the UdpReceiver class.
#include "receiver.h"

// Constructor for the UdpReceiver class.
UdpReceiver::UdpReceiver(unsigned short port)
    : socket_(io_context_, asio::ip::udp::endpoint(asio::ip::udp::v4(), port)),
    received_datagrams_(0),
    min_delay_ms_(std::numeric_limits<double>::max()),
    max_delay_ms_(std::numeric_limits<double>::min()) {}

// Start receiving datagrams.
void UdpReceiver::start_receiving() {
    receive_next_datagram();
    io_context_.run();
}

// Stop receiving datagrams.
void UdpReceiver::stop_receiving() {
    io_context_.stop();
}

// Get the number of received datagrams.
unsigned int UdpReceiver::get_received_datagrams() const {
    return received_datagrams_;
}

// Get the minimum delay between received datagrams (in milliseconds).
double UdpReceiver::get_min_delay_ms() const {
    return min_delay_ms_;
}

// Get the maximum delay between received datagrams (in milliseconds).
double UdpReceiver::get_max_delay_ms() const {
    return max_delay_ms_;
}

// Receive the next datagram.
void UdpReceiver::receive_next_datagram() {
    socket_.async_receive_from(asio::null_buffers(), sender_endpoint_,
        [this](std::error_code ec, std::size_t) {
            // If there's no error, increment the received_datagrams counter,
            // calculate the delay between received datagrams, and call receive_next_datagram again.
            if (!ec) {
                ++received_datagrams_;
                auto now = std::chrono::steady_clock::now();
                if (received_datagrams_ > 1) {
                    double delay = std::chrono::duration<double, std::milli>(now - last_received_time_).count();
                    min_delay_ms_ = std::min(min_delay_ms_, delay);
                    max_delay_ms_ = std::max(max_delay_ms_, delay);
                }
                last_received_time_ = now;
                receive_next_datagram();
            }
        });
}