#include <iostream>
#include <signal.h>
#include "receiver.h"

UdpReceiver* g_receiver = nullptr;

void signal_handler(int) {
    if (g_receiver) {
        g_receiver->stop_receiving();
    }
}

int main() {
    UdpReceiver receiver(8080);
    g_receiver = &receiver;

    signal(SIGINT, signal_handler);

    receiver.start_receiving();

    std::cout << "received datagrams: " << receiver.get_received_datagrams() << std::endl;
    std::cout << "delay min/max: " << receiver.get_min_delay_ms() << "/"
        << receiver.get_max_delay_ms() << " ms" << std::endl;

    return 0;
}