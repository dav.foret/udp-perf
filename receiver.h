#pragma once

#include <asio.hpp>
#include <chrono>
#include <limits>

class UdpReceiver {
public:
    UdpReceiver(unsigned short port);

    void start_receiving();
    void stop_receiving();

    unsigned int get_received_datagrams() const;
    double get_min_delay_ms() const;
    double get_max_delay_ms() const;

private:
    void receive_next_datagram();

    asio::io_context io_context_;
    asio::ip::udp::socket socket_;
    asio::ip::udp::endpoint sender_endpoint_;
    std::chrono::steady_clock::time_point last_received_time_;
    unsigned int received_datagrams_;
    double min_delay_ms_;
    double max_delay_ms_;
};