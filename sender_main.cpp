#include <iostream>
#include <signal.h>
#include "sender.h"

UdpSender* g_sender = nullptr;

void signal_handler(int) {
    if (g_sender) {
        g_sender->stop_sending();
    }
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cerr << "Usage: send <target_ip> <period_ms>" << std::endl;
        return 1;
    }

    std::string target_ip = argv[1];
    int period_ms = std::stoi(argv[2]);

    UdpSender sender(target_ip, 8080, period_ms);
    g_sender = &sender;

    signal(SIGINT, signal_handler);

    sender.start_sending();

    std::cout << "sent datagrams: " << sender.get_sent_datagrams() << std::endl;

    return 0;
}